package exceptions;

import java.util.Scanner;

public class ExceptionsApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Press Ctrl+C to terminate");
        String[] words;
        while (true) {
            String text = scanner.nextLine();
            words = text.split("[^А-Яа-яa-zA-Z_0-9\\-]+");
            for (String word : words) {
                if (word.length() > 10) {
                    try {
                        throw new SoLongWordException();
                    } catch (SoLongWordException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}